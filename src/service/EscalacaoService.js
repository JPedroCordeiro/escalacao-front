import axios from 'axios'
import { WS_ESCALACAO } from './EndPoints'

const escalacaoService = {
    
    async findAll() {
        let response = await axios.get(WS_ESCALACAO)
        return response.data
    },
    
    async save(escalacao) {
        await axios.post(WS_ESCALACAO, escalacao)
    }
}

export default escalacaoService