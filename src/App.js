import React from 'react'
import { BrowserRouter, Link } from 'react-router-dom'

import AppRouter from './ui/router/AppRouter'

function App() {

  return (
    <BrowserRouter>
      <div>
        <Link to="/escalacao">Escalação</Link>
      </div>

      <AppRouter />

    </BrowserRouter>
  );
}

export default App;
